var jp
class faseMinhoca extends fasePeixe {

    preload() {
	game.load.spritesheet("background", 'assets/bg_worm.png',413,500)
	game.load.spritesheet('ring', 'assets/rings.png', 64, 64)
	game.load.spritesheet('peixe',"assets/peixe_inimigo.png", 64, 127)
	game.load.spritesheet('bather',"assets/banhista.png", 32, 47)
	game.load.image("mihoca", "assets/worm3.png", 168,132)
	game.load.spritesheet("game_over","assets/game_over_minhoca2.png" ,140,192)
	game.load.image("boot","assets/boot.png",808,800)

    }

    create(){
	this.objects = []
	this.initBackground()
	console.log("FASE_MINHOCA")
	console.log(game.lifes)
	this.createPlayer("mihoca", "top", game.lifes, "down", 1, this)
	console.log("call to prototype")
	this.__proto__.__proto__.create(
	    {customPlayer:true,
	     customBackground:true}
	)
	console.log("FAsemimnohca")
	console.log(game.lifes)
	this.canJump = false
	game.world.bringToTop(this.player)
	this.mapMatrix = processMap(textMap["fase_minhoca"],this.makeSprites())
    	this.objSpeed = 1000
    	this.objScaleSpeed = 1000
	this.objectDelay = 1200
    	this.laneNo = 3
    	this.speed = 1000
    	this.jmpHeight = 200
	game.input.onTap.add(this.onTap,this)
	this.nextMap = "fase_peixe"
	this.player.inputEnabled=true;
	this.player.input.enableDrag();
    }

    update(){
	for(let o of this.objects){
	    game.physics.arcade.overlap(
	    this.player, o, this.hit, null, this);
	}
	this.sendMap()
	this.player.body.velocity.x = 0
    }

    initBackground(){
	//Create the background
	this.background = game.add.sprite(0, 0, 'background')
	this.background.height = game.world.height;
	this.background.width = game.world.width;
	this.background.animations.add("bganimation",makeArray(0,50))
	this.background.animations.play("bganimation", 60, true)
    }

    makeSprites(){
	var ringAnim = function(s){
	    s.animations.add("spin",makeArray(0,30), true)
	    s.animations.play("spin", 200, true)
	}

	var ringmaker =
	    abstractCreate('ring',
			   {scale:[1.5,1.5]},
			   {points:100,
			    lifes:0,
			    visible:false},
			  ringAnim)

	var addFishAnim = function (s){
	    s.animations.add("flap", [0,1], 5, true)
	    s.animations.play("flap")
	}
	var peixemaker =
	    abstractCreate('peixe',
			   {scale:[1.5,1.5]},
			   {points:0,
			    lifes:-1,
			    visible:false,
			    onCollide:this.hitBaddie},
			   addFishAnim)
	var bootmaker =
	    abstractCreate(
		"boot",
		{scale:[0.1,0.1]},
		{points:0,
		 lifes:-1,
		 visible:false,
		 onCollide:this.hitBaddie}
	    )

	var addHeartAnim = function(s){
	    s.animations.add("bump", makeArray(0,4), 4,true)
	    s.animations.play('bump')
	    s.body.setSize(64,64,32,32)
	}

	var heartmaker =
	    abstractCreate(
		'heart',
		{},
		{points:0,
		 lifes:1,
		 visible:false},
		addHeartAnim)
	
	return {c:ringmaker, b:peixemaker, t:bootmaker, h:heartmaker}
    }

    playerDeath(){
	this.player.kill()
	let spr = createSprite("game_over",
		     {x:game.world.centerX,
		      y:0,
		      anchor:[0.5, 0],
		      scale:[1.5,1.5]})
	let anim = spr.animations.add("end", makeArray(0,30), 15)
	anim.onComplete.add(
	    ()=> game.state.start("game_over")
			   )
	anim.play("end")	
    }


    sendObjects(objlist){
	if(objlist.length != this.laneNo){
	    console.log("Voce so pode enviar "+this.laneNo+" objetos por vez")
	    console.log("objlist é:")
	    console.log(objlist)
	    return}

	for(let i = 0; i<this.laneNo; i++){
	    let o = objlist[i];
	    if(o){
		this.objects.push(o)
		o.visible = true
		this.applyTween(o,i)

	    }}}

    applyTween(o,i){
	// configuraçao inicial
	o.x = this.laneEndings[i]
	o.y = game.height
	
	//Tweening da posicao
	let tween = game.add.tween(o)
	let endTween = game.add.tween(o)
	tween.to(
	    {y:0},
	    this.objSpeed,
	    Phaser.Easing.Exponential.In,true
	)

	endTween.to(
	    {y:-this.player.height},
	    this.objSpeed,
	    Phaser.Easing.Circular.In
	).onComplete.add(()=>o.kill())
	
	tween.onComplete.add(()=>{
	    endTween.start()
	})


    }


    createLanes(){
	var centerY = game.height/2
	var centerX = game.width/2
	for(var x = 0; x < this.laneNo; x++){
	    this.laneEndings.push( (x*this.laneSpacing) + this.laneSpacing/2 )
	}
    }

    render(){
	// for(let x of this.objects){
	//     game.debug.body(x)	    
	// }


    // 	game.debug.geom( new Phaser.Circle(x,game.height, 50), 'rgba(255,255,255)')
    }
}
