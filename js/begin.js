class begin extends Phaser.State{
    preload(){
    }

    create(){

	var nameLabel = game.add.text(
	    game.world.centerX,	80,
	    'Freseza\nFiŝhisto',
	    { font: '50px Arial', fill: '#ffe63b' });
	nameLabel.anchor.x = 0.5;

	var startLabel = game.add.text(
	    game.world.centerX, game.world.height,
	    'Toque\nou\naperte\n"G"\npara recomeçar',
	    {font: '25px Arial', fill: '#ffffff' });
	startLabel.anchor.x = 0.5;
	startLabel.y = game.world.height - 2 * startLabel.height

	var wkey = game.input.keyboard.addKey(
	    Phaser.Keyboard.G);

	game.score = 0
	wkey.onDown.addOnce(
	    () => game.state.start(game.primeira_fase)
	    , this);

	game.input.addPointer()
	game.lifes = 15
	game.input.onTap.add(
	    (pointer,doubletap)=> game.state.start("fase_carro"),
	    this)
    }

    fullScreen(){
	game.scale.fullScreenScaleMode = Phaser.ScaleManager.EXACT_FIT
	game.scale.startFullScreen()	
	game.state.start("game_over")

    }
}



