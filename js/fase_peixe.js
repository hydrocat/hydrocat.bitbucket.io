var jp
var play
class fasePeixe extends Phaser.State{

    controls(){
	if (keyboard.jump.isDown){
	    this.jump()
	}
	else{
	    this.jumpIsAllow = true
	}

	if (keyboard.left.isDown){
	    this.player.body.velocity.x = this.speed * -1
	}

	if (keyboard.right.isDown){
	    this.player.body.velocity.x = this.speed
	}

	let o = gyro.getOrientation();
	if(o.x){
	    this.player.x = Math.floor(game.world.width -(((o.x + 10)/2) * game.world.width)/10)
	    //20 is the max reading of the sensor
	}
    }
    
    jump(){
	if (this.jumpIsAllow && this.canJump){
	    this.jmp.start()
	    this.jumpIsAllow = false
	}
    }

    onTap(poiter, doubleTap){
		this.jump()
    }
    
    preload() {
		game.load.spritesheet("background", 'assets/bg2.jpg',810,810)
		game.load.spritesheet('ring', 'assets/rings.png', 64, 64)
		game.load.spritesheet('bather',"assets/banhista.png", 32, 47)
		game.load.spritesheet('boss', "assets/boss.png", 79, 77)
		game.load.spritesheet("shark", "assets/sharkonido.png", 140,75)
		game.load.spritesheet("fish", "assets/peixe.png", 100,70)
    }

    
    create(){
	this.objectStartPosition = 115
	this.objectDelay = 700
	this.objSpeed = 1000
	this.objScaleSpeed = 1000
	this.endScale = 2.3
	this.laneNo = 3
	this.speed = 1000
	this.jumpIsAllow = true
	this.jmpHeight = 200
	this.canJump = true
	if( arguments[0].customMap != true){
	    this.mapMatrix =
		processMap(textMap["fase_peixe"], this.makeSprites())}

	if( arguments[0].customBackground != true){
	    this.initBackground()
	    console.log("using standard background initializaer")
	} 
	
	this.bossg = game.add.group()
	this.blocked = false
	this.coinsg = game.add.group()
	this.centerX = game.world.centerX
	this.centerY = game.world.centerY
	game.renderer.roundPixels = true
	game.renderer.clearBeforeRender = true

	if(arguments[0].customLane != true){
	    this.laneEndings = []
	    this.laneHeight = game.height*0.75
	    this.laneSpacing  = game.width/this.laneNo
	    this.createLanes()
	}

	//Creating the player fish sprite
	if( arguments[0].customPlayer != true ){
	    this.createPlayer("fish", "bottom", game.lifes, "up", 3, this)
	    this.player.body.setCircle(28, 22, 13)
	    game.input.onTap.add(this.onTap,this)
	}

	let lifes = game.lifes;
	game.scoreLabel = game.add.text(
	    10, 10,
	    "Score: "+ game.score + "\nLifes: "+ lifes,
	    { font: '30px Arial', fill: '#ffffff' }
	)

	this.nextMap = "win"

    }

    hitBaddie(){
	navigator.vibrate(100)
	game.camera.shake(0.01,100)
    }
    hitBoss(){
	navigator.vibrate(300)
	game.camera.shake(0.1,300)
    }
    
    makeSprites(){
	var ringAnim = function(s){
	    s.animations.add("spin",makeArray(0,30), true)
	    s.animations.play("spin", 200, true)
	}
	var bossAnim = function(s){
		s.animations.add("move", makeArray(0,7), true)
		s.animations.play("move", 10, true)
	}
	this.sharkAnim = function(s){
		s.animations.add("bite", makeArray(0,4), true)
		s.animations.play("bite", 5, false)
	}
	var ringmaker =
	    abstractCreate('ring',
			   {scale:[2,2]},
			   {points:100,
			    lifes:0,
			    visible:false},
			   ringAnim)
	var batherAnim = function(b){
	    b.animations.add("",[0,1,2,3],16, true)
	    b.animations.play("")
	}
	var bathermaker =
	    abstractCreate('bather',
			   {scale:[3,3]},
			   {points:0,
			    lifes:-1,
			    visible:false,
			    onCollide:this.hitBaddie},
			   batherAnim)
	var bossMaker =
	    abstractCreate('boss',
			   {},
			   {boss:true,
			    scx:2,
			    scy:2,
			    life:3,
			    lastShot:0,
			    visible:false,
			    lifes:-1,
			    points:0,
			    onCollide:this.hitBoss},
				bossAnim)
	return {b: bathermaker, c:ringmaker, B:bossMaker}
    }

    createPlayer(animName,
		 face,
		 lifes,
		 jumpDirection,
		 jumpMultiplier,
		 that,
		 createArgs = {params:{},
			       extra:{},
			       fun:(e)=>{}}
		){

	/*
	  jumpDirection is either "up" "down" "left" "right"
	  jumpMultiplier is how high the player will jump
	  face is either "top" "bottom" "left" "right"
	*/
	this.player =
	    createSprite(animName,
			 createArgs.params,
			 createArgs.extra,
			 createArgs.fun)


	play = this.player
	let jumpTweenTable = {
	    "up":
	    {y:game.height-this.player.height * jumpMultiplier},

	    "down":
	    {y:this.player.height * jumpMultiplier},

	    "left":
	    {x:game.width-this.player.width * jumpMultiplier},

	    "right":
	    {x:this.player.width * jumpMultiplier}
	}

	let posTable = {
	    "top":
	    {x:game.width/2, y:this.player.height/2},

	    "bottom":
	    {x:game.width/2, y:game.height-this.player.height/2},

	    "left":
	    {x:game.height/2, y:this.player.width/2},

	    "right":
	    {x:game.height/2, y:game.width -this.player.width/2},
	}
	if(that == undefined){
	    that = this
	}
	that.player.position = posTable[face]
	that.player.body.collideWorldBounds = true
//	that.player.lifes = 3

	that.jmp = game.add.tween(that.player)
	that.jmp.to(
	    jumpTweenTable[jumpDirection],
	    170,
	    Phaser.Easing.Sinusoidal.Out,
	    false, 0, 0, true)
	return that.player
    }

    initBackground(){
	//Create the background
	this.background = game.add.sprite(0, 0, 'background')
	this.background.height = game.world.height;
	this.background.width = game.world.width;
	this.background.animations.add("bganimation",makeArray(0,24))
	this.background.animations.play("bganimation", 15, true)
    }

    update(){
	this.sendMap()
	this.player.body.velocity.x = 0
	this.controls()
	// Para o mapa se tiver boss no grupo
	if (this.bossg.length > 0){
	    this.blocked = true
	    this.bossg.forEach((item)=>{
		if(game.time.time - item.lastShot > 700 && item.shot){
		    this.bossShot(item)
		}
	    })
	}
	else{
	    this.blocked = false
	}
	
	game.physics.arcade.collide(
	    this.player,
	    this.bossg,
	    (player, boss)=>{
		boss.body.enable = false
		boss.visible = false
		boss.life += 1
		this.hit(this.player, boss)
	})

    }


    hit(player, obj){
	game.score += obj.points
	game.lifes+= obj.lifes
	game.scoreLabel.setText("Score: "+game.score+"\nLifes: " + this.game.lifes)
	if (obj.onCollide){
	    obj.onCollide()
	}
	if (!obj.boss){
	    obj.destroy()
	}
	if (game.lifes <= 0){
	    game.lifes = 3
	    this.playerDeath()
	}

    }

    playerDeath(){
	game.state.start("game_over")	
    }

    sendMap(){
		if (!this.lastSendMap){
			this.lastSendMap = game.time.time+this.objectDelay
		}
	if ((game.time.time - this.lastSendMap) < this.objectDelay || this.blocked){
	    return
	}
	if( this.mapMatrix.length ){
	    var x =	this.mapMatrix.pop()
	    for (const [i, v] of x.entries()){
		if (v){
		    if (v.boss){
			x[i] =  null
			this.bossInit(v)
		    }
		    else{
			this.coinsg.add(v) 
		    }
		}
	    }
	    this.sendObjects(x)
	    this.lastSendMap = game.time.time
	} else {
	    game.state.start(this.nextMap)
	}
    }

    bossShot(b){
	let shot = createSprite('shark',{}, {lifes:-1,
					      points:0,	
					      visible:false,
					      onCollide: ()=>{
						  navigator.vibrate([100])
						  game.camera.shake(0.01, 100)
					      }
					     }, this.sharkAnim)

	shot.body.setSize(shot.body.width*0.5, shot.body.height*0.5, 0,shot.body.height*0.5)
	let lane = this.whichLane(b.x)
	let array = []
	for (let i = 0; i < this.laneNo; i++){
	    array.push(null)
	}
	array[lane] = shot
	this.sendObjects(array)
	b.lastShot = game.time.time
    }

    whichLane(x){
	for (let i = 0; i < this.laneNo; i++){
	    if (x <= this.laneEndings[i]){
		return i
	    }

	}
    }

    bossInit(b){
	b.visible = true
	b.body.enable = false
	b.shot = true
	this.bossg.add(b)
	// movimentação do BOSS
	this.background.tint = 0xff0033
	b.y = game.world.centerY
	b.x = this.laneEndings[0]
	b.scale.setTo(0.5,0.5)
	//Tweening para o objeto sair da tela
	let moveTween = game.add.tween(b).to(
	    {x:this.laneEndings[this.laneNo-1]},
	    this.objSpeed,null, false, 0, 1, true
	)


	moveTween.onComplete.add(()=>{
	    
	    bossJumpTween.start()

	})

	let bossJumpTween = game.add.tween(b).to(
	    {y:-15}, this.objSpeed, Phaser.Easing.Exponential.Out, false, 0
	)
	bossJumpTween.onComplete.add(()=>{
	    b.scale.setTo(b.scx, b.scy)
	    b.x = this.player.x
	    b.y = 0
	    b.body.enable = true
	    b.shot = false
	    bossFallTween.start()
	})

	let bossFallTween = game.add.tween(b).to(
	    {y: game.world.height}, this.objSpeed, Phaser.Easing.Bounce.Out
	)
	bossFallTween.onComplete.add(()=>{
	    
	    b.life -= 1
	    if (b.life <= 0){
		b.destroy()
		this.background.tint = 0xffffff
		game.state.start("win")
	    }
	    else{
		this.bossInit(b)				
	    }
	})
	moveTween.start()
    }

    sendObjects(objlist){
	if(objlist.length != this.laneNo){
	    console.log("Voce so pode enviar "+this.laneNo+" objetos por vez")
	    console.log("objlist é:")
	    console.log(objlist)
	    return}

	for(let i = 0; i<this.laneNo; i++){
	    let o = objlist[i];
	    if(o){
		o.visible = true
		this.applyTween(o,i)

	    }}}

    applyTween(o,i){
	// configuraçao inicial
	o.x = this.laneEndings[i]//game.world.centerX
	o.y = game.world.centerY //+ this.objectStartPosition

	//Tweening da escala
	game.add.tween(o.scale).from(
	    {x:0.1, y:0.1},
	    this.objScaleSpeed,
	    Phaser.Easing.Circular.In,true)

	//Tweening para o objeto sair da tela
	let endTween = game.add.tween(o).to(
	    {y:game.height+o.height*100},
	    this.objSpeed/5
	)
	endTween.onComplete.add(()=>o.kill())

	//Tweening da posicao
	game.add.tween(o).to(
	    {x:this.laneEndings[i]
	     , y:game.height-o.height*10},
	    this.objSpeed,
	    Phaser.Easing.Exponential.In,true
	).onComplete.add(()=>{
	    game.physics.arcade.overlap(this.player, o, this.hit, null, this)
	    endTween.start()})
    }

    createLanes(){
	var centerY = game.height/2
	var centerX = game.width/2
	for(var x = 0; x < this.laneNo; x++){
	    this.laneEndings.push( (x*this.laneSpacing) + this.laneSpacing/2 )
	}
    }

    

    render(){

			//game.debug.body(play)
	/*		for (var x of this.laneEndings){
			game.debug.geom( new Phaser.Circle(x,game.height, 50), 'rgba(255,255,255)')
			}
	*/
    }
}
