var i
var debug
class gameOver extends Phaser.State{
    preload(){

	game.load.image("arrow", "assets/arrow.png")
    }

    create(){

	var nameLabel = game.add.text(
	    game.world.centerX,	80,
	    'Game Over',
	    { font: '50px Arial', fill: '#ff0000' });
	nameLabel.anchor.x = 0.5;

	var startLabel = game.add.text(
	    game.world.centerX, game.world.height-80,
	    'Toque\nou\naperte\n"G"\npara recomeçar',
	    {font: '25px Arial', fill: '#ffffff' });
	startLabel.anchor.x = 0.5;
	startLabel.y = game.world.height - 2 * startLabel.height

	var wkey = game.input.keyboard.addKey(
	    Phaser.Keyboard.G);

	game.score = 0
	wkey.onDown.addOnce(
	    () => game.state.start("begin")
	    , this);

	game.input.addPointer()
	game.score = 15
	game.input.onTap.add(
	    (pointer,doubletap)=> game.state.start("begin"),
	    this)
    }

    fullScreen(){
	game.scale.fullScreenScaleMode = Phaser.ScaleManager.EXACT_FIT
	game.scale.startFullScreen()	
	game.state.start("game_over")

    }
}



