var i
var debug
class win extends Phaser.State{
	preload(){

		game.load.video("recipe", "cutscenes/finish.mp4")
	}

	create(){
		var group = game.add.group();
		var video = game.add.video('recipe')
		var sprite = group.create(game.world.centerX, game.world.centerY, video);
		sprite.anchor.set(0.5);
		let scale = game.world.width/sprite.width
	//	sprite.width = game.world.width
	//	sprite.height = game.world.height
		sprite.scale.set(scale, scale)
	//	video.onPlay.addOnce(start, this);
	//	video.addToWorld(game.world.centerX, game.world.centerY, 0.5, 0.5,);
		video.play(true);

	var nameLabel = game.add.text(
		game.world.centerX,	50,
		'Você ganhou!',
		{ font: '40px Arial', fill: '#ff0000' });
	nameLabel.anchor.x = 0.5;
	nameLabel.anchor.y = 0.5;

	var startLabel = game.add.text(
		game.world.centerX, game.world.height-50,
		'Pontuação: ' + game.score,
		{font: '25px Arial', fill: '#ffffff' });
	startLabel.anchor.x = 0.5;
	startLabel.anchor.y = 0.5;

	var wkey = game.input.keyboard.addKey(
		Phaser.Keyboard.G);

	game.score = 0
	wkey.onDown.addOnce(
		() => game.state.start("begin")
		, this);

	game.input.addPointer()
	game.input.onTap.add(
		(pointer,doubletap)=> game.state.start("begin"),
		this)
	}

	fullScreen(){
	game.scale.fullScreenScaleMode = Phaser.ScaleManager.EXACT_FIT
	game.scale.startFullScreen()	
	game.state.start("game_over")

	}
}
