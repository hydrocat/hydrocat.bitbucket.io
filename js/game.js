'use strict'

function makeArray(min,max){
    let array = []
    let crescente = min<max
    let x = crescente ? min : max;
    for( let x = min;
    	 crescente ? x < max : x > max;
    	 crescente ? x++ : x--){
    	array.push(x)
    }
    return array
}

var game = new Phaser.Game(window.innerWidth-20, window.innerHeight-20, Phaser.CANVAS, 
    'game-container', {
    //    preload: preload, 
      //  create: create,
      //  update: update, 
       // render: render
    }
)

game.primeira_fase = "fase_carro"
game.score = 0
game.lifes = 15
//game.state.add('boot', bootState)
game.state.add('begin',begin)
game.state.add('menu', menu)
game.state.add('fase_peixe', fasePeixe)
game.state.add('fase_minhoca', faseMinhoca)
game.state.add("load", load)
game.state.add("game_over", gameOver)
game.state.add("fase_carro", faseCarro)
game.state.add("win", win)
//game.state.add('win', winState)



game.state.start("load")
//game.state.start("fase_peixe")


