class menu extends Phaser.State {
  
    create() {
		
        var nameLabel = game.add.text(80, 80, 'La Freneza Fiŝisto',
                                    { font: '50px Arial', fill: '#ffffff' });
        
        // We give the player instructions on how to start the game
        var startLabel = game.add.text(80, game.world.height-80,
                                       'Pressione "W" para começar',
                                       {font: '25px Arial', fill: '#ffffff' });
        
        // We define the wkey as Phaser.Keyboard.W so that we can act
        // when the player presses it
        var wkey = game.input.keyboard.addKey(Phaser.Keyboard.W);
        
        // When the player presses the W key, we call the start function
        wkey.onDown.addOnce(this.start, this);
    }
    
    // The start function calls the play state    
    start() {
       game.state.start('fase_peixe');    
    }   
}
