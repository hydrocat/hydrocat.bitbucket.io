function processMap(map, sprites){
    //map.reverse()
    let ret = []
    let delay = 0
    map.forEach((item, index)=>{
	let lineAux = []
	item.split("").forEach((it, idx)=>{
	    if(it == ' ' ){
		lineAux.push(null)
	    }
	    else if (typeof sprites[it] == "function"){
		lineAux.push(sprites[it]())
	    } else {
		console.log("Não sei criar sprites para \"" + it + "\",  sprites = ")
		console.log(sprites)
		lineAux.push(null)
	    }
	})
	ret.push(lineAux)
    })
    return ret
}


class Map {

	constructor(textMap){
		this.matrix = this.processMap(textMap)
	}

	processMap(map){
		//map.reverse()
		let ret = []
		let delay = 0
		let createRing = abstractCreate('ring',{scale:[2,2]},{points:100, lifes:0, visible:false})
	    let createBather = abstractCreate(
		'bather',
		{scale:[3,3],
		 onCollide: ()=>{
		     
		 }
		},{points:0, lifes:-1, visible:false,
		   onCollide: ()=>{
		       navigator.vibrate([100])
		       game.camera.shake(0.01, 100)
		   }})
	    
		map.forEach((item, index)=>{
			let lineAux = []
			item.split("").forEach((it, idx)=>{
				if (it == 'c'){
				    let coin = createRing()
        			    coin.animations.add("spin",makeArray(0,30), true)
/*					//Tweening da posicao
					game.add.tween(coin).to(
					{x:game.width/2, y:game.height},
					1000,
					Phaser.Easing.Exponential.In,true, delay)
					delay += 500*/
        			coin.animations.play("spin", 200, true)
					lineAux.push(coin)
					
				}
				
				else if(it == 'b'){
					let bather = createBather()
        			bather.animations.add("",[0,1,2,3],16, true)
	       			bather.animations.play("")
					lineAux.push(bather)

				}
				else if (it == 'B'){
					lineAux.push(createBoss())
				}
				else{

					lineAux.push(null)
				}
			})
			ret.push(lineAux)
		})
		return ret
	}
}
