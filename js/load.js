var keyboard
class load extends Phaser.State{

    constructor(){
	super()
    }

    preload() {
	game.load.image('fullscreen-button', 'assets/fullscreen-button.png')
	game.load.spritesheet("heart", "assets/heart.png", 128,128)
	//Start the physics engine
	game.physics.startSystem(Phaser.Physics.ARCADE)


	//game.load.image('food', 'assets/food.png')
	//game.load.spritesheet('skull_coin', 'assets/skull_coin.png', 100, 100)

	keyboard =  game.input.keyboard.addKeys({
	    'left': Phaser.Keyboard.A, 
	    'right': Phaser.Keyboard.L, 
	    'jump': Phaser.Keyboard.SPACEBAR
	}
					       )
    }

    create () {

 	let fullScreenIcon =
	    this.game.add.sprite(
		this.game.width/2,
		this.game.height/2,
		'fullscreen-button')
	
        fullScreenIcon.anchor.setTo(0.5, 0.5)
        fullScreenIcon.scale.setTo(0.75, 0.75)
        fullScreenIcon.inputEnabled = true
        fullScreenIcon.events.onInputDown.add(this.fullScreen, this)            
	game.world.bringToTop(fullScreenIcon);

	game.state.start('begin');
    }



	fullScreen(){
		game.scale.fullScreenScaleMode = Phaser.ScaleManager.EXACT_FIT
		game.scale.startFullScreen()	
		game.state.start("game_over")

	}
}
