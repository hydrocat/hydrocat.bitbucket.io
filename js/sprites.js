// create an sprite (double, double, name, list, list)
function createSprite(name,params={}, extra={}, fun = (e)=>{}){
	let conf = {
		x:0, y:0,
		scale:[1,1],
		anchor:[0.5, 0.5]
		}

	for( let i in params ){
		conf[i] = params[i]
	}

    let sprite
    sprite = game.add.sprite(conf.x, conf.y, name)
    sprite.anchor.setTo(conf.anchor[0], conf.anchor[1])
    sprite.scale.setTo(conf.scale[0],conf.scale[1])
    game.physics.arcade.enable(sprite)

	for (let i in extra){
		sprite[i] = extra[i]
	}

    fun(sprite)

    return sprite
}

function abstractCreate(name,params,extra,fun){
    return () => createSprite(name,params,extra,fun)}

