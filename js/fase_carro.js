var jp
class faseCarro extends fasePeixe {

    preload() {
	game.load.spritesheet("background", 'assets/road.png',140,200)
	game.load.spritesheet('ring', 'assets/rings.png', 64, 64)
	game.load.image('log',"assets/log.png", 800, 160)
	game.load.spritesheet('bather',"assets/banhista.png", 32, 47)
	game.load.image("mihoca", "assets/worm3.png", 168,132)
	game.load.image("carro", "assets/carro.png", 360, 800)
	game.load.spritesheet("blow", "assets/blow.png", 256, 256)
	game.load.spritesheet("game_over","assets/game_over_minhoca2.png" ,140,192)

    }

    onTap(pointer,doubleTap){
	if(this.lane){
	    this.lane = 0
	} else {
	    this.lane = 1
	}
	this.player.x = this.laneEndings[this.lane]
    }

    controls(){}

    create(){
	this.initBackground()
	this.lane = 0
	let createArgs = {params:{scale:[-0.2,-0.2]},
			  extra:{},
			  fun:(e)=>{}}
	this.createPlayer("carro",
			  "bottom",
			  game.lifes,
			  "right",
			  1,
			  this,createArgs)
	this.laneNo = 2
	this.laneEndings = []
	this.laneSpacing  = game.width/this.laneNo
	this.createLanes()
	this.__proto__.__proto__.create(
	    {customPlayer:true,
	     customBackground:true,
	     customMap:true,
	     cutsomLanes:true}
	)
	this.mapMatrix =
	    processMap(textMap["fase_carro"], this.makeSprites())

	this.canJump = false
	game.world.bringToTop(this.player)
    	this.objectDelay = 400
    	this.objSpeed = 1000
    	this.objScaleSpeed = 1000
    	this.speed = 1000
    	this.jmpHeight = 200
	game.input.onTap.add(this.onTap,this)
	this.nextMap = "fase_minhoca"
    }


    update(){
	this.sendMap()
	this.player.body.velocity.x = 0
	this.controls()
    }

    initBackground(){
	//Create the background
	this.background = game.add.sprite(0, 0, 'background')
	this.background.height = game.world.height;
	this.background.width = game.world.width;
	this.background.animations.add("bganimation",makeArray(0,5))
	this.background.animations.play("bganimation", 5, true)
    }

    makeSprites(){
	var ringAnim = function(s){
	    s.animations.add("spin",makeArray(0,30), true)
	    s.animations.play("spin", 200, true)
	}

	var ringmaker =
	    abstractCreate('ring',
			   {scale:[1.5,1.5]},
			   {points:100,
			    lifes:0,
			    visible:false},
			  ringAnim)
	var logmaker =
	    abstractCreate('log',
			   {scale:[0.2,0.2]},
			   {points:0,
			    lifes:-1,
			    visible:false,
			    onCollide:this.hitBaddie})
	var addHeartAnim = function(s){
	    s.animations.add("bump", makeArray(0,4), 4,true)
	    s.animations.play('bump')
	    s.body.setSize(64,64,32,32)
	}

	var heartmaker =
	    abstractCreate(
		'heart',
		{},
		{points:0,
		 lifes:1,
		 visible:false},
		addHeartAnim)
	
	return {c:ringmaker, b:logmaker, l:heartmaker}
    }
    

    playerDeath(){
	let fun = (e) =>{
	    let anim = e.animations.add(
		"blow up",
		makeArray(0,32).concat(makeArray(32,0)),
		60,
	    )
	    anim.onComplete.add(
		()=> game.state.start("game_over")
	    )
	    e.animations.play("blow up")
	}
	let spr = createSprite("blow",
			       {x:this.player.x,
				y:this.player.y},
			       {},
			       fun)

	this.player.kill()
    }

    applyTween(o,i){
	// configuraçao inicial
	o.x = this.laneEndings[i]
	o.y = 0
	
	//Tweening da posicao
	let tween = game.add.tween(o)
	let endTween = game.add.tween(o)
	tween.to(
	    {y:game.world.height - ( -1*this.player.height )},
	    this.objSpeed,
	    Phaser.Easing.Linear.In,true
	)

	endTween.to(
	    {y:game.world.height + o.height},
	    this.objSpeed /1.5 ,
	    Phaser.Easing.Linear.Out
	).onComplete.add(()=>{o.kill()})
	
	tween.onComplete.add(()=>{
	    game.physics.arcade.overlap(
		this.player, o, this.hit, null, this);
	    endTween.start()
	})


    }


    createLanes(){
	var centerY = game.height/2
	var centerX = game.width/2
	for(var x = 0; x < this.laneNo; x++){
	    this.laneEndings.push( (x*this.laneSpacing) + this.laneSpacing/2 )
	}
    }

    render(){
	// game.debug.body(this.player)
/*
			game.debug.geom( new Phaser.Circle(x,game.height, 50), 'rgba(255,255,255)')
			}
	*/
    }
}
